# Le module turtle

- **Fiche élève cours :** Découvrir_le_module_turtle.ipynb
- **Fiche élève activité :** exercices_module_turtle + projet_module_turtle
- **Déroulé :** deroule_module_turtle
--- 

**Thématique :** Les bases de la programmation

**Notions liées :** fonctions et boucles for

**Résumé de l’activité :** découverte du module turtle - Utilisation pour réaliser des figures - Mini-projet

**Objectifs :** savoir écrire un programme en python, 
				savoir utiliser une boucle for pour répéter une instruction 
				et une fonction pour répéter un bloc d'instructions

**Durée de l’activité :** 1h environ pour le notebook, 3h pour les exercices et 3h pour le mini-projet.

**Forme de participation :** découverte et exercices en travail individuel sur poste, projet réalisé en binôme.

**Matériel nécessaire :** ordinateurs avec interprèteur Python.

**Préparation : Les notions de variables et de type ont été étudiés lors d'un premier chapitre sur la programmation

**Autres Références :** 

- Maxime Fourny (académie Franche-Comté)
