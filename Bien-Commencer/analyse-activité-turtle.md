# **Analyse de l’activité turtle**

## **Objectif**
* Découvrir la programmation en Python et le module turtle en lien avec la programmation avec Scratch.   
* Découvrir (ou redécouvrir) l’utilisation des boucles en Python et mettre en place la notion de fonction.

## **Prérequis**
Au collège, les élèves ont découvert les commandes de déplacement et de tracé grâce au langage Scratch et sont normalement déjà familiarisés avec ce type de programmes. Les notions de boucle et de variable ont du être introduites au travers de la programmation de dessins.  
Cette activité est une activité de début d’année de seconde : 
* première séance de programmation avec Scratch dans laquelle on réutilise une boucle et une fonction sur un programme donné  
* puis présentation de l’interface de Python  
* cette activité peut alors être une première activité de programmation en Python.  

## **Durée**
3 séances d’une heure.  

## **Exercices cibles**
Ex 2, 4,  6, 12 et 15.  

## **Description du déroulement de l’activité**
* 1ère séance : ex 1 à 11
Présentation du module turtle puis travail autonome en binôme sur les exercices 1 à 11.
Quand tous les binômes ont cherché l’ex 6, présentation de la fonction dir() pour obtenir toutes les fonctions prédéfinies dans le module turtle et notamment la fonction goto pour faire (peut-être autrement) les ex 7 et 8.  
* 2ème séance : ex 12 à 17
Présentation par un ou plusieurs groupes d’élèves de leurs programmes de la séance 1 pour les exercices 4, 6, 10.  
Programmation de l’ex 12 avec Scratch.  
Introduction de la boucle for et de la notion de fonction puis programmation de l’ex 12 en Python en classe entière.  
Ex 13 à 17 travail autonome en binôme.  
* 3ème séance : ex 18 à20
Présentation par un ou plusieurs groupes d’élèves de leurs programmes de la séance 2 pour les exercices 14 et 15.  
Travail autonome en binôme sur la suite des exercices.  

## **Anticipation des difficultés**
Les difficultés rencontrées seront traitées au fur et à mesure de l’avancée dans le travail.
(angles à prendre en compte lors des déplacements, orientation et sens du déplacement, les boucles et l'utilisation de fonctions).  

## **Gestion de l’hétérogénéité**
Pour les plus lents, suppression des derniers exercices de chacune des séances ce qui ne les pénalisera pas par rapport aux attendus de l’activité.

## **Mon analyse** 
Le module turtle est un moyen concret et assez ludique d’introduire le langage Python et a l’avantage de ne pas s’appuyer sur des concepts ou des connaissances mathématiques. On ne multiplie donc pas les difficultés.   
Le deuxième avantage est que les élèves ont le visuel de leur programme, et voit directement si il y a des erreurs.  
L’activité est un peu longue mais elle peut permettre d’introduire ou de revenir sur plusieurs concepts importants de programmation.  

